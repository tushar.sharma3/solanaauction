import * as anchor from "@project-serum/anchor";
import { Program } from "@project-serum/anchor";
import { Auction } from "../target/types/auction";
import {
  TOKEN_PROGRAM_ID,
  MINT_SIZE,
  createAssociatedTokenAccountInstruction,
  getAssociatedTokenAddress,
  createInitializeMintInstruction,
  createBurnInstruction,
  createBurnCheckedInstruction,
  burn,
  burnChecked,
} from "@solana/spl-token";
import { Keypair, LAMPORTS_PER_SOL, Connection, Transaction, SystemProgram } from "@solana/web3.js";
import { assert } from "chai";

describe("auction", async () => {
  // Configure the client to use the local cluster.
  anchor.setProvider(anchor.AnchorProvider.env());

  const program = anchor.workspace.Auction as Program<Auction>;
  const mintKey: anchor.web3.Keypair = anchor.web3.Keypair.generate();
  // AssociatedTokenAccount for anchor's workspace wallet
  let associatedTokenAccount = undefined;

  let connection = new Connection("https://api.devnet.solana.com", "confirmed");


  let secretKey = await Uint8Array.from(JSON.parse(
    require("fs").readFileSync("/root/.config/solana/id.json")
  ));

  let payerkeypair = Keypair.fromSecretKey(secretKey);
  let nftDetails = Keypair.generate();

  let signer = Keypair.generate();

  it("Mint a token", async () => {
    // Get anchor's wallet's public key
    const key = anchor.AnchorProvider.env().wallet.publicKey;
    // Get the amount of SOL needed to pay rent for our Token Mint
    const lamports: number = await program.provider.connection.getMinimumBalanceForRentExemption(
      MINT_SIZE
    );

    // Get the ATA for a token and the account that we want to own the ATA (but it might not existing on the SOL network yet)
    associatedTokenAccount = await getAssociatedTokenAddress(
      mintKey.publicKey,
      key
    );

    // Fires a list of instructions
    const mint_tx = new anchor.web3.Transaction().add(
      // Use anchor to create an account from the mint key that we created
      anchor.web3.SystemProgram.createAccount({
        fromPubkey: key,
        newAccountPubkey: mintKey.publicKey,
        space: MINT_SIZE,
        programId: TOKEN_PROGRAM_ID,
        lamports,
      }),
      // Fire a transaction to create our mint account that is controlled by our anchor wallet
      createInitializeMintInstruction(
        mintKey.publicKey, 0, key, key
      ),
      // Create the ATA account that is associated with our mint on our anchor wallet
      createAssociatedTokenAccountInstruction(
        key, associatedTokenAccount, key, mintKey.publicKey
      )
    );

    // sends and create the transaction
    const res = await anchor.AnchorProvider.env().sendAndConfirm(mint_tx, [mintKey]);

    // Executes our code to mint our token into our specified ATA
    await program.methods.mintToken().accounts({
      mint: mintKey.publicKey,
      tokenProgram: TOKEN_PROGRAM_ID,
      tokenAccount: associatedTokenAccount,
      authority: key,
    }).rpc();

    // Get minted token amount on the ATA for our anchor wallet
    const minted = (await program.provider.connection.getParsedAccountInfo(associatedTokenAccount)).value.data.parsed.info.tokenAmount.amount;
    // assert.equal(minted, 1);
    console.log(minted);


  });

  // it("Transfer token", async () => {
  //   // Get anchor's wallet's public key
  //   const myWallet = anchor.AnchorProvider.env().wallet.publicKey;
  //   // Wallet that will receive the token 
  //   const toWallet: anchor.web3.Keypair = anchor.web3.Keypair.generate();
  //   // The ATA for a token on the to wallet (but might not exist yet)
  //   const toATA = await getAssociatedTokenAddress(
  //     mintKey.publicKey,
  //     toWallet.publicKey
  //   );

  //   // Fires a list of instructions
  //   const mint_tx = new anchor.web3.Transaction().add(
  //     // Create the ATA account that is associated with our To wallet
  //     createAssociatedTokenAccountInstruction(
  //       myWallet, toATA, toWallet.publicKey, mintKey.publicKey
  //     )
  //   );

  //   // Sends and create the transaction
  //   await anchor.AnchorProvider.env().sendAndConfirm(mint_tx, []);

  //   let airdropSignature1 = await connection.requestAirdrop(
  //     associatedTokenAccount,
  //     LAMPORTS_PER_SOL
  //   );


  //   const tx = await program.methods.transferToken().accounts({
  //     tokenProgram: TOKEN_PROGRAM_ID,
  //     from: associatedTokenAccount,
  //     to: toATA,
  //     fromAuthority: payerkeypair.publicKey,
  //   }).signers([payerkeypair]).rpc();


  //   // Get minted token amount on the ATA for our anchor wallet
  //   const minted = (await program.provider.connection.getParsedAccountInfo(associatedTokenAccount)).value.data.parsed.info.tokenAmount.amount;
  //   assert.equal(minted, 0);
  //   const minted1 = (await program.provider.connection.getParsedAccountInfo(toATA)).value.data.parsed.info.tokenAmount.amount;
  //   assert.equal(minted1,1);

  //   console.log("Signature:",tx);

  // });

  //   it("Burn a token", async () => {
  //     let tx = await new Transaction().add(
  //       createBurnInstruction(
  //         associatedTokenAccount, // token account
  //         mintKey.publicKey, // mint
  //         payerkeypair.publicKey, // owner of token account
  //         5, // amount, if your deciamls is 8, 10^8 for 1 token
  //         [payerkeypair],
  //         TOKEN_PROGRAM_ID // decimals
  //       )
  //     );

  //     // const res = await /
  //     // console.log(tx);
  //     // const minted1 = (await program.provider.connection.getParsedAccountInfo(associatedTokenAccount)).value.data.parsed.info.tokenAmount.amount;
  //     // console.log(minted1);
  //     // assert.equal(minted1, 10);
  //     console.log("151");
  //     console.log({
  //       mint: mintKey.publicKey.toBase58(),
  //       tokenProgram: TOKEN_PROGRAM_ID.toBase58(),
  //       tokenAccount: associatedTokenAccount.toBase58(),
  //       authority: payerkeypair.publicKey.toBase58(),
  //       from:payerkeypair.publicKey.toBase58()
  //     });
  //     const burnNfts = await program.methods.burn().accounts({
  //       mint: mintKey.publicKey,
  //       tokenProgram: TOKEN_PROGRAM_ID,
  //       tokenAccount: associatedTokenAccount,
  //       authority: payerkeypair.publicKey,
  //       from:payerkeypair.publicKey
  //     }).signers([payerkeypair]).rpc();

  //     const minted = (await program.provider.connection.getParsedAccountInfo(associatedTokenAccount)).value.data.parsed.info.tokenAmount.amount;
  //     console.log(minted);
  //     assert.equal(minted, 10);
  //   });



  it("Create Auction", async () => {
    const lamports: number = await program.provider.connection.getMinimumBalanceForRentExemption(
      MINT_SIZE
    );
    await program.methods.createAuction(new anchor.BN(1000)).
      accounts({
        nftDetails: nftDetails.publicKey,
        user: payerkeypair.publicKey,
        systemProgram: SystemProgram.programId,
        mint: mintKey.publicKey,
        tokenAccount: associatedTokenAccount
      }).
      signers([nftDetails]).rpc();

    console.log("Sharma");
    console.log({
      nftDetails: payerkeypair.publicKey.toBase58(),
      user: nftDetails.publicKey.toBase58(),
      systemProgram: SystemProgram.programId.toBase58(),
      mint: mintKey.publicKey.toBase58(),
      tokenAccount: associatedTokenAccount.toBase58()
    });
    const getData = await program.account.nftDetails.fetch(nftDetails.publicKey);
    console.log(getData);
  })


});
