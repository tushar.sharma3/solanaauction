use anchor_lang::prelude::*;
use anchor_spl::token;
use anchor_spl::token::{MintTo, Token, Transfer};

declare_id!("FSn7PyMnZNVAN4sHR1synHAZkqQjQgAMgurNGwNRU4UR");

#[program]
pub mod auction {
    use super::*;
    pub fn mint_token(ctx: Context<MintToken>) -> Result<()> {
        // Create the MintTo struct for our context
        let cpi_accounts = MintTo {
            mint: ctx.accounts.mint.to_account_info(),
            to: ctx.accounts.token_account.to_account_info(),
            authority: ctx.accounts.authority.to_account_info(),
        };

        let cpi_program = ctx.accounts.token_program.to_account_info();
        // Create the CpiContext we need for the request
        let cpi_ctx = CpiContext::new(cpi_program, cpi_accounts);

        // Execute anchor's helper function to mint tokens
        token::mint_to(cpi_ctx, 1)?;

        Ok(())
    }

    pub fn transfer_token(ctx: Context<TransferToken>) -> Result<()> {
        // Create the Transfer struct for our context
        let transfer_instruction = Transfer {
            from: ctx.accounts.from.to_account_info(),
            to: ctx.accounts.to.to_account_info(),
            authority: ctx.accounts.from_authority.to_account_info(),
        };

        let cpi_program = ctx.accounts.token_program.to_account_info();
        // Create the Context for our Transfer request
        let cpi_ctx = CpiContext::new(cpi_program, transfer_instruction);

        // Execute anchor's helper function to transfer tokens
        anchor_spl::token::transfer(cpi_ctx, 1)?;

        Ok(())
    }
    
 
    //create nfts
    pub fn create_auction(ctx:Context<CreateAuction>,price:u64)-> Result<()>{
        let nft_details = &mut ctx.accounts.nft_details;
       
        nft_details.owner = *ctx.accounts.user.key;
        nft_details.token_id = *ctx.accounts.mint.key;
        nft_details.token_account = *ctx.accounts.token_account.key;
        nft_details.owner_authority = *ctx.accounts.owner_authority.key;
        nft_details.token_price = price;
        Ok(())
    }

   // pub fn bid(ctx: Context<Bid>,bid: u64) -> Result <()>{
    //     let my_bid = BiderFields{
    //         bidder: ctx.accounts.bidder.key(),
    //         bid,
    //     };
    //     // ctx.accounts.nft_details.bids;
    //     Ok(())
  
    // }
    // pub fn Accept_bid(ctx: Context<AcceptBid>) -> Result <()>{


    //     Ok(())

    // }
    // pub fn Reject_bid(ctx: Context<RejectBid>) -> Result <()>{
    //     Ok(())
    //}


}

#[derive(Accounts)]
pub struct MintToken<'info> {
    /// CHECK: This is the token that we want to mint
    #[account(mut)]
    pub mint: UncheckedAccount<'info>,
    
    pub token_program: Program<'info, Token>,
    /// CHECK: This is the token account that we want to mint tokens to
    #[account(mut)]
    pub token_account: UncheckedAccount<'info>,
    /// CHECK: the authority of the mint account
    #[account(mut)]
    pub authority: AccountInfo<'info>,
}

#[derive(Accounts)]
pub struct TransferToken<'info> {
    pub token_program: Program<'info, Token>,
    /// CHECK: The associated token account that we are transferring the token from
    #[account(mut)]
    pub from: UncheckedAccount<'info>,
    /// CHECK: The associated token account that we are transferring the token to
    #[account(mut)]
    pub to: AccountInfo<'info>,
    // the authority of the from account
    pub from_authority: Signer<'info>,
}

#[derive(Accounts)]
pub struct CreateAuction <'info>{
    #[account(init, payer = user, space = 264)]
    pub nft_details: Account<'info,NFTDetails>,
    
    #[account(mut)]
    pub user: Signer<'info>,

    #[account(mut)]
    pub owner_authority: Signer<'info>,

    pub system_program: Program<'info,System>,
    /// CHECK: This is the token that we want to mint
    #[account(mut)]
    pub mint: UncheckedAccount<'info>,
    /// CHECK: This is the token that we want to mint
    #[account(mut)]
    pub token_account: UncheckedAccount<'info>,
}




#[derive(Accounts)]
pub struct TransferNft<'info> {

    /// CHECK: We're about to create this with Anchor
    #[account(mut)]
    pub bidder_token_account: UncheckedAccount<'info>,
    #[account(mut)]
    pub bidder_authority: Signer<'info>,

    // to Create Accociated Token Address
    pub rent: Sysvar<'info, Rent>,
    // pub system_program: Program<'info, System>,
    pub token_program: Program<'info, token::Token>,
    pub associated_token_program: Program<'info, associated_token::AssociatedToken>,
}





#[derive(Accounts)]
pub struct Bid <'info>{
    #[account(mut)]
    pub nft_details: Account<'info,NFTDetails>,   
    /// CHECK: This is the token that we want to mint
    pub bidder: UncheckedAccount<'info> 

}


#[derive(Accounts)]
pub struct AcceptBid <'info>{
    #[account(mut)]
    pub nft_details: Account<'info,NFTDetails>,
    
}


// #[derive(Accounts)]
// pub struct RejectBid <'info>{
//     #[account(mut)]
//     pub nft_details: Account<'info,NFT_Details>,
// }

#[account]
pub struct NFTDetails{
    pub owner: Pubkey,
    pub token_id: Pubkey,
    pub token_account: Pubkey,
    pub owner_authority:Pubkey,
    pub token_price: u64,
    pub bids: HashMap<StructAddrss, Bid>,
}

#[account]
pub struct BiderFields{
    pub bidder: Pubkey,
    pub bid: u64,
}



